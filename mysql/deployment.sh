kubectl apply -f ~/k4-kubernetes/mysql/mysql-config.yaml
kubectl apply -f ~/k4-kubernetes/mysql/mysql-secret.yaml
kubectl apply -f ~/k4-kubernetes/mysql/mysql-pv.yaml
kubectl apply -f ~/k4-kubernetes/mysql/mysql-pvc.yaml
kubectl apply -f ~/k4-kubernetes/mysql/mysql-deployment.yaml
kubectl apply -f ~/k4-kubernetes/mysql/mysql-ip-cluster-service.yaml
