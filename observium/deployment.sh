kubectl apply -f ~/k4-kubernetes/observium/observium-config.yaml
kubectl apply -f ~/k4-kubernetes/observium/observium-secret.yaml
kubectl apply -f ~/k4-kubernetes/observium/observium-pv-logs.yaml
kubectl apply -f ~/k4-kubernetes/observium/observium-pv-rrd.yaml
kubectl apply -f ~/k4-kubernetes/observium/observium-pvc-logs.yaml
kubectl apply -f ~/k4-kubernetes/observium/observium-pvc-rrd.yaml
kubectl apply -f ~/k4-kubernetes/observium/observium-deployment.yaml
kubectl apply -f ~/k4-kubernetes/observium/observium-node-port.yaml
